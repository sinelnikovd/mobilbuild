$(document).ready(function() {
  $(".homeset .carousel").slick({
    arrows: false,
    dots: true,
    infinite: true,
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    useCSS: true,
    useTransform: true,
    speed: 1500,
    autoplay: true,
    customPaging : function(slider, i) {
        var num = (i < 10) ? "0" + (i+1) : (i+1);
        return '<button type="button">'+ num +'</button>';
      },
  }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
    $(slick.$slides[currentSlide]).addClass("anim-out");
    $(slick.$slides[nextSlide]).addClass("anim-in");
  }).on('afterChange', function(event, slick, currentSlide) {
    $(slick.$slides[currentSlide]).removeClass("anim-in");
    for(var i = 0; i < slick.$slides.length; i++){
      $(slick.$slides[i]).removeClass("anim-out");
    }
  });

  var controller = new ScrollMagic.Controller();

  // build scenes
  new ScrollMagic.Scene({triggerElement: "#line1"})
          .setClassToggle("#line1", "active")
          .addTo(controller)
          .on("start", function (event) {
            event.target.duration($(event.target.triggerElement()).height());
          });
  new ScrollMagic.Scene({triggerElement: "#line2"})
          .setClassToggle("#line2", "active")
          .addTo(controller)
          .on("start", function (event) {
            event.target.duration($(event.target.triggerElement()).height());
          });
  new ScrollMagic.Scene({triggerElement: "#line3"})
          .setClassToggle("#line3", "active")
          .addTo(controller)
          .on("start", function (event) {
            event.target.duration($(event.target.triggerElement()).height());
          });
  new ScrollMagic.Scene({triggerElement: "#line4"})
          .setClassToggle("#line4", "active")
          .addTo(controller)
          .on("start", function (event) {
            event.target.duration($(event.target.triggerElement()).height());
          });
  new ScrollMagic.Scene({triggerElement: "#line5"})
          .setClassToggle("#line5", "active")
          .addTo(controller)
          .on("start", function (event) {
            event.target.duration($(event.target.triggerElement()).height());
          });
  new ScrollMagic.Scene({triggerElement: "#line6"})
          .setClassToggle("#line6", "active")
          .addTo(controller)
          .on("start", function (event) {
            event.target.duration($(event.target.triggerElement()).height());
          });

  var slideout = new Slideout({
    panel: document.getElementById('panel'),
    menu: document.getElementById('menu'),
    padding: 256,
    tolerance: 70,
    side: 'right'
  });

  $('#toggle-menu-button').on('click', function() {
    slideout.toggle();
  });

  function close(eve) {
    eve.preventDefault();
    slideout.close();
  }

  slideout
    .on('beforeopen', function() {
      this.panel.classList.add('panel-open');
    })
    .on('open', function() {
      this.panel.addEventListener('click', close);
    })
    .on('beforeclose', function() {
      this.panel.classList.remove('panel-open');
      this.panel.removeEventListener('click', close);
    });

  $(".js-video-popup").magnificPopup({
    type: 'iframe',
    iframe: {
      patterns: {
        youtube: {
          index: 'youtube.com/',
          id: 'v=',
          src: '//www.youtube.com/embed/%id%?autoplay=1'
        },
      },
    }
  });

  $('#videbg').vide({
      mp4: 'video/bg.mp4',
      poster: 'video/bg.jpg'
    },{
      bgColor: "#11222d",
      posterType: "jpg"
    });

});